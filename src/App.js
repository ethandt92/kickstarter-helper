import React, { Component } from 'react';
import cx from 'classnames'
import logo from './logo.svg';
import './App.css';
import normalizeUrl from 'normalize-url'
import {Map, List} from 'immutable'
import ReactGA from 'react-ga'

const WEBTASK_ID = 'wt-ethan-joinronin-com-0'
const GA_ID = 'UA-90342573-1'

ReactGA.initialize(GA_ID)

const fetchWebtask = async (path, body) => {
  try {
    let response = await fetch(`https://${WEBTASK_ID}.run.webtask.io${path}`, {
      method: 'POST',
      body: JSON.stringify(body)
    })
    response = await response.json()
    if (response.error) {
      console.error(`${response.error}, ${response.message}`)
      return null
    }
    return response
  } catch (error) {
    console.error(error)
  }
}

const fetchKickstarter = params => fetchWebtask('/kickstarter', params)
const fetchIndiegogo = params => fetchWebtask('/indiegogo', params)
const fetchGoogle = params => fetchWebtask('/google', params)
const fetchBing = params => fetchWebtask('/bing', params)
const fetchBuzzstream = params => fetchWebtask('/buzzstream', params)
const fetchAlexa = params => fetchWebtask('/alexa', params)
const fetchFacebook = params => fetchWebtask('/facebook', params)
const fetchSharedcount = params => fetchWebtask('/sharedcount', params)
const fetchSharedcountBulkPost = params => fetchWebtask('/sharedcountbulkpost', params)
const fetchSharedcountBulkGet = params => fetchWebtask('/sharedcountbulkget', params)
const fetchProducthunt = params => fetchWebtask('/producthunt', params)

function unique(arr) {
  var hash = {}, result = [];
  for (const item of arr) {
    if (!hash.hasOwnProperty(item)) {
      hash[item] = true;
      result.push(item);
    }
  }
  return result;
}

const TIMEOUT = 1000

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
async function sleep(ms, fn, ...args) {
  await timeout(ms)
  return fn(...args)
}

const EXCLUDED_SITES = [
  'kickstarter',
  'google',
  'wikipedia',
  'facebook',
  'yahoo',
  'imgur',
  'youtube',
  'quora',
  'amazon',
  'stackexchange',
  'dummies',
  'seedandspark',
  'kicktraq',
  'runcrowd',
  'webrobots',
  'backerkit'
]

const EXCLUDED_RELATED_SITES = [
  ...EXCLUDED_SITES,
  'twitter',
  'pinterest',
  'linkedin',
  't.co',
  'instagram',
  'wikia',
  'etsy',
  'reddit'
]

const EXCLUDE_Q = '-site:kickstarter.com -site:indiegogo.com'

const NORMALIZE_URL_OPTIONS = {normalizeProtocol: false}

function includesStr(arr, str) {
  for (const item of arr) {
    if (str.includes(item))
      return true
  }
  return false
}

function getParameterByName(name, url) {
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

const initialState = {
  data: Map({
    items: List(),
    displayForItem: Map(),
    statsForItem: Map(),
    canDownload: false,
    canSubmit: true,
    canceled: false,
    downloaded: false,
    progress: 0,
    progressTotal: 0,
    kickstarterSort: 'most_funded',
    page: 1,
    count: 10,
    start: 0
  })
}

class App extends Component {
  state = initialState
  setStatePromise = (newState) => {
    return new Promise((resolve) => this.setState(newState, () => resolve()))
  }
  setImmStatePromise = (newState) => {
    return new Promise((resolve) => this.setImmState(newState, () => resolve()))
  }
  setImmState(fn) {
    return this.setState(({data}) => ({
      data: fn(data)
    }));
  }
  createImmGetterAndSetter = (key) => (value, ...args) => {
    if (value === undefined)
      return this.state.data.get(key)
    this.setImmState(d => d.set(key, value))
  }
  items = this.createImmGetterAndSetter('items')
  displayForItem = this.createImmGetterAndSetter('displayForItem')
  statsForItem = this.createImmGetterAndSetter('statsForItem')
  canDownload = this.createImmGetterAndSetter('canDownload')
  canSubmit = this.createImmGetterAndSetter('canSubmit')
  canceled = this.createImmGetterAndSetter('canceled')
  downloaded = this.createImmGetterAndSetter('downloaded')
  progress = this.createImmGetterAndSetter('progress')
  progressTotal = this.createImmGetterAndSetter('progressTotal')
  kickstarterSort = this.createImmGetterAndSetter('kickstarterSort')
  page = this.createImmGetterAndSetter('page')
  count = this.createImmGetterAndSetter('count')
  start = this.createImmGetterAndSetter('start') 
  onSubmit = async (event) => {
    event.preventDefault()
    const value = this.input.value
    ReactGA.event({
      category: 'Home',
      action: 'Search',
      label: value
    })
    if (!this.canSubmit() || !value)
      return
    this.canDownload(false)
    this.canSubmit(false)
    // if (!this.canceled())
    //   await this.queryCrowdfundingItems(value)
    // if (!this.canceled())
    //   await this.querySearchItems(`${value} kickstarter|indiegogo ${EXCLUDE_Q}`)
    if (!this.canceled())
      await this.queryAuthorItems(value)
    // if (!this.canceled())
    //   await this.processItems(this.items(), this.queryRelatedItems)
    this.canDownload(true)
    this.canSubmit(true)
    this.canceled(false)
  }
  queryCrowdfundingItems = async (q) => {
    console.log('queryCrowdfundingItems', q)
    if (this.canceled()) 
      return
    let result = await this.fetchKickstarter(q)
    if (result && result.projects)
      await this.processItems(result.projects, this.processKickstarterItem)
    else
      console.error('fetchKickstarter returned no results')
    if (this.canceled()) 
      return
    result = await this.fetchIndiegogo(q)
    if (result && result.campaigns)
      await this.processItems(result.campaigns, this.processIndiegogoItem)
    else
      console.error('fetchIndiegogo returned no results')
  }
  querySearchItems = async (q) => {
    console.log('querySearchItems', q)
    if (this.canceled()) 
      return
    let result = await this.fetchGoogle(q)
    if (result && result.items)
      await this.processItems(result.items, this.processGoogleItem)
    else
      console.error('fetchGoogle returned no results')

    if (this.canceled()) 
      return
    result = await this.fetchBing(q)
    if (result && result.webPages && result.webPages.value)
      await this.processItems(result.webPages.value, this.processBingItem)
    else
      console.error('fetchBing returned no results')
  }
  queryAuthorItems = async (q) => {
    console.log('queryAuthorItems', q)
    if (this.canceled()) 
      return

    let result  
    
    // result = await this.fetchBuzzstream(q)
    // if (result && result.authors)
    //   await this.processItems(result.authors, this.processBuzzstreamItem)
    // else
    //   console.error('fetchBuzzstream returned no results')
    
    if (this.canceled()) 
      return

    result = await fetchProducthunt({q})
    console.log(result)
    if (result && result.hits)
      await this.processItems(result.hits, this.processProducthuntPostItem)
    else
      console.error('fetchProducthunt returned no results')
  }
  queryRelatedItems = async (site) => {
    console.log('queryRelatedItems', site)
    if (this.isExcludedRelatedItem(site) || this.canceled())
      return
    let result = await this.fetchAlexa(site)
    if (result && result.results)
      await this.processItems(result.results, this.processAlexaItem)
    else
      console.error('fetchAlexa returned no results')
  }
  querySiteStats = async (site) => {
    console.log('querySiteStats', site)
    let returnValue = {
      facebook: 0,
      stumbleupon: 0,
      google: 0,
      twitter: 0,
      pinterest: 0,
      linkedin: 0,
      shares: 0
    }
    let result

    // if (this.canceled()) 
    //   return returnValue

    // let result = await fetchFacebook({id: site})
    // if (!result)
    //   console.error('fetchFacebook returned no results')
    // else if (result.error) 
    //   console.error(result.error.message)
    // else if (!result.share) 
    //   console.error('facebook share undefined')
    // else {
    //   const {share:{share_count}} = result
    //   returnValue.facebook = share_count
    // }
    
    if (this.canceled()) 
      return returnValue

    result = await fetchSharedcount({url: site})
    if (!result)
      console.error('fetchSharedcount returned no results')
    else if (result.Error)
      console.error(`${result.Error}, ${result.Type}`)
    else {
      const {
        StumbleUpon, 
        Facebook, 
        GooglePlusOne, 
        Twitter, 
        Pinterest, 
        LinkedIn
      } = result
      returnValue.stumbleupon = StumbleUpon || returnValue.stumbleupon
      returnValue.google = GooglePlusOne || returnValue.google
      returnValue.twitter = Twitter || returnValue.twitter
      returnValue.pinterest = Pinterest || returnValue.pinterest
      returnValue.linkedin = LinkedIn || returnValue.linkedin
      returnValue.shares += GooglePlusOne + Twitter + Pinterest + LinkedIn
      if (Facebook) {
        const {
          commentsbox_count,
          click_count,
          total_count,
          comment_count,
          like_count,
          share_count
        } = Facebook
        returnValue.facebook = share_count
        returnValue.shares += share_count
      }
    }
      
    return returnValue
  }
  fetchKickstarter = async (term) => {
    console.log('fetchKickstarter', term)
    return await fetchKickstarter({
      term,
      sort: this.kickstarterSort(),
      page: this.page()
    })
  }
  fetchIndiegogo = async (q) => {
    console.log('fetchIndiegogo', q)
    return await fetchIndiegogo({
      q,
      page: this.page(),
      per_page: this.count()
    })
  }
  fetchGoogle = async (q) => {
    console.log('fetchGoogle', q)
    return await fetchGoogle({
      q,
      start: this.start()
    })
  }
  fetchBing = async (q) => {
    console.log('fetchBing', q)
    return await fetchBing({
      q,
      count: this.count(),
      offset: this.start()
    })
  }
  fetchBuzzstream = async (q) => {
    console.log('fetchBuzzstream', q)
    return await fetchBuzzstream({
      q,
      from: this.start()
    })
  }
  fetchAlexa = async (site) => {
    console.log('fetchAlexa', site)
    return await fetchAlexa({
      site
    })
  }
  processItems = async (items, cb) => {
    if (items) {
      this.addProgressTotal(items.length || items.size || 0)
      for (const item of items) {
        await timeout(TIMEOUT)
        if (this.canceled())
          break
        await cb(item)
        this.incrementProgress()
      }
    }
  }
  processKickstarterItem = async (item) => {
    console.log('processKickstarterItem', item)
    await this.processCrowdfundingItem(`${item.name} kickstarter ${EXCLUDE_Q}`)
  }
  processIndiegogoItem = async (item) => {
    console.log('processIndiegogoItem', item)
    await this.processCrowdfundingItem(`${item.title} indiegogo ${EXCLUDE_Q}`)
  }
  processCrowdfundingItem = async (q) => {
    console.log('processCrowdfundingItem', q)
    await this.querySearchItems(q)
  }
  processGoogleItem = async (item) => {
    console.log('processGoogleItem', item)
    await this.processSearchItem(item.displayLink, item.link)
  }
  processBingItem = async (item) => {
    console.log('processBingItem', item)
    let actualUrl = decodeURIComponent(getParameterByName('r', item.url))
    await this.processSearchItem(item.displayUrl, actualUrl)
  }
  processSearchItem = async (display, actual) => {
    console.log('processSearchItem', display, actual)
    await this.processSiteItem(display, actual)
    // await this.queryRelatedItems(display)
  }
  processBuzzstreamItem = async (item) => {
    console.log('processBuzzstreamItem', item)
    this.processAuthorItem(item.name, `//idb.buzzstream.com${item.url}`)
    await this.processItems(item.relevant_articles, this.processBuzzstreamArticle)
    await this.processItems(item.sites, this.processBuzzstreamSite)
  }
  processBuzzstreamArticle = async (item) => {
    console.log('processBuzzstreamArticle', item)
    await this.processSiteItem(item.url, item.url)
    // await this.queryRelatedItems(item.url)
  }
  processBuzzstreamSite = async (item) => {
    console.log('processBuzzstreamSite', item)
    if (item.twitter_name)
      await this.processSiteItem('//twitter.com', `//twitter.com/${item.twitter_name}`)
    else
      await this.processSiteItem(item.domain, item.domain)
    // await this.queryRelatedItems(item.domain)
  }
  processAuthorItem = async (name, url) => {
    console.log('processAuthorItem', name, url)
    url = normalizeUrl(url, NORMALIZE_URL_OPTIONS)
    if (this.isExistingItem(url))
      return
    this.appendItem(name, url)
  }
  processProducthuntPostItem = async (item) => {
    console.log('processProducthuntPostItem', item)
    if (item.author && !item.author.is_maker) {
      await this.processPostItem(item.author.name || item.author.username, `//producthunt.com/${item.url}`, item.vote_count)
    } else if (item.user && !item.user.is_maker) {
      await this.processPostItem(item.user.name || item.user.username, `//producthunt.com/${item.url}`, item.vote_count)
    }
  }
  processPostItem = async (name, url, votes) => {
    console.log('processPostItem', name, url, votes)
    url = normalizeUrl(url, NORMALIZE_URL_OPTIONS)
    if (this.isExistingItem(url))
      return
    this.appendItem(name, url, {votes})
  }
  processAlexaItem = async (item) => {
    console.log('processAlexaItem', item)
    if (item.overlap_score)
      await this.processRelatedItem(item.site2)
  }
  processRelatedItem = async (site) => {
    console.log('processRelatedItem', site)
    if (site)
      await this.processSiteItem(site, site)
  }
  processSiteItem = async (display, actual) => {
    console.log('processSiteItem', display, actual)
    display = normalizeUrl(display, NORMALIZE_URL_OPTIONS)
    actual = normalizeUrl(actual, NORMALIZE_URL_OPTIONS)
    if (this.isExcludedItem(display) || this.isExistingItem(actual))
      return
    if (actual.includes('twitter.com'))
      this.appendTwitterItem(actual)
    else {
      await timeout(TIMEOUT)
      const stats = await this.querySiteStats(actual)
      this.appendItem(display, actual, stats)
    }
  }
  isExistingItem = (item) => includesStr(this.items(), item)
  isExcludedItem = (item) => includesStr(EXCLUDED_SITES, item)
  isExcludedRelatedItem = (item) => includesStr(EXCLUDED_RELATED_SITES, item)
  appendTwitterItem = (url) => {
    const handle = url.match(/((https?:\/\/)?(www\.)?twitter\.com\/)?(@|#!\/)?([A-Za-z0-9_]{1,15})(\/([-a-z]{1,20}))?/g)[5]
    if (handle)
      this.appendItem('@' + handle, url)
  }
  appendItem = (display, url, stats) => {
    this.setImmState(d => 
      d
        .update('items', list => list.push(url))
        .update('displayForItem', map => map.set(url, display))
        .update('statsForItem', map => map.set(url, stats))
    )
  }
  incrementProgress = () => {
    this.setImmState(d => d.update('progress', v => v + 1))
  }
  addProgressTotal = (amount) => {
    this.setImmState(d => d.update('progressTotal', v =>  v + amount))
  }
  download = () => {
    ReactGA.event({
      category: 'Home',
      action: 'Download'
    })

    const items = this.items()
    const displayForItem = this.displayForItem()
    const statsForItem = this.statsForItem()

    if (!this.canDownload())
      return
      
    let csvContent = 'data:text/csv;charset=utf-8,'
    items.forEach((url, index) => {
      const displayName = displayForItem.get(url)
      const stats = statsForItem.get(url)
      const dataString = `${displayName},${url},${stats.facebook}`
      csvContent += index < items.size ? dataString + '\n' : dataString
    })

    const link = document.createElement('a')
    link.setAttribute('href', encodeURI(csvContent))
    link.setAttribute('download', 'download.csv')
    document.body.appendChild(link)
    link.click()

    this.downloaded(true)
  }
  cancel = () => {
    ReactGA.event({
      category: 'Home',
      action: 'Cancel'
    })
    this.canceled(true)
  }
  restart = () => {
    ReactGA.event({
      category: 'Home',
      action: 'Restart'
    })
    this.setState(initialState)
  }
  renderSite = (url, i) => {
    const displayName = this.displayForItem().get(url)
    const stats = this.statsForItem().get(url)
    let statsContent = ' 0 shares'
    if (!stats)
      statsContent = ' n/a shares'
    else if (stats.shares)
      statsContent = ` ${stats.shares} shares`
    else if (stats.votes) 
      statsContent = ` ${stats.votes} votes`
    return <p key={url} className="paragraph">
      <a 
        target='_blank'
        href={url} 
        className={cx('Site-link', { glink: displayName !== url })}>
        {displayName}
      </a>
      {statsContent}
    </p>
  }
  renderDownloadButton() {
    const canDownload = this.canDownload()
    return canDownload ? <div className="ctas" onClick={this.download}>
      <button className="ctas-button ctas-button-dark" disabled={!canDownload}>Download</button>
    </div>: null
  }
  renderProgress() {
    const progress = this.progress()
    const progressTotal = this.progressTotal()
    return progressTotal > 0 ?
    <p className="paragraph">Progress: {progress}/{progressTotal}</p>
    : null
  }
  render() {
    const canSubmit = this.canSubmit()
    const canDownload = this.canDownload()
    const downloaded = this.downloaded()
    const items = this.items()
    return (
      <div className="App">
        <header className="header">
          <div className="container-lrg">
            <div className="col-12 spread">
              <div>
                <a className="logo">
                  Kickstarter Helper
                </a>
              </div>
            </div>
          </div>
          <div className="container-sml">
            <div className="col-12 text-center">
              <h1 className="heading">
                Find Relevant Bloggers
              </h1>
              <h2 className="paragraph">
                Searches for projects similar to yours and returns websites talking about those projects.
              </h2>
              <form className="ctas" onSubmit={this.onSubmit}>
                <label>
                  <input 
                    className="ctas-input" 
                    type="text" 
                    ref={(input) => this.input = input} 
                    placeholder="boardgame wallet app etc." 
                    disabled={!canSubmit} 
                    />
                </label>
                {canSubmit ? 
                  <input 
                    className="ctas-button"
                    type="submit" 
                    value="Search" 
                    disabled={!canSubmit}
                    />
                : null}
              </form>
              {!canSubmit ? 
                <h2 className="paragraph">
                  Loading...
                </h2>: 
              null}
            </div>
          </div>
        </header>
        <div className="feature6">
          <div className="container-sml flex">
            <div className="col-12">
              <h3 className="heading">
                Results
              </h3>
              {!canSubmit ? 
                <div className="ctas" onClick={this.cancel}>
                  <button className="ctas-button ctas-button-dark" disabled={canSubmit}>Cancel</button>
                </div> 
              : null}
              {canDownload ? 
                <div className="ctas" onClick={this.restart}>
                  <button className="ctas-button ctas-button-dark" disabled={!canDownload}>Restart</button>
                </div> 
              : null}
              {this.renderDownloadButton()}
              {this.renderProgress()}
              {items.map(this.renderSite)}
              {this.renderProgress()}
              {this.renderDownloadButton()}
              {downloaded ?
                <div>
                  <h1 className="heading">
                    What do I do now?
                  </h1>
                  <p className="paragraph">
                    Read more in Tools of Titans and on <a href="//fourhourworkweek.com/kickstarter" target='_blank'>Tim's blog</a>
                  </p>
                </div>
              : null}
            </div>
          </div>
        </div>
        <div className="footer">
          <div className="container-sml">
            <div className="col-12 text-center">
              <div>
                <a className="nav-link" href="//joinronin.com" target='_blank'>joinronin.com</a>
              </div>
              <br />
              <div>
                <span>Made with React, Launchaco, Webtask</span>
              </div>
              <br />
              <div>
                <span>Inspired by Tim Ferriss and Tools of Titans</span>
              </div>
              <div>
                <span>Minimum Effective Dose for Kickstarter Traffic</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
