const BodyUndefinedError = new Error('Body undefined');
const BulkIDTypeError = new Error('bulk_id type should be string');

const ENDPOINT = "https://free.sharedcount.com"
const PATH = "/bulk"
const API_KEY = "b488e797f58ed208e4ba1a9c7f4d493b81727663"

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?bulk_id=${params.bulk_id}&apikey=${API_KEY}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.bulk_id !== 'string') {
        cb(BulkIDTypeError);
        return;
    }
    typeFetch(body, cb);
}