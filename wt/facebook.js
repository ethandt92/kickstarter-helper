
const BodyUndefinedError = new Error('Body undefined');
const IdTypeError = new Error('id type should be string');

const ENDPOINT = "https://graph.facebook.com"
const PATH = ""
const APP_ID = "1774124426246471"
const APP_SECRET = "f5acfd2b70754b03a773ba46d3079853" 

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?id=${params.id}&access_token=${APP_ID}|${ACCESS_TOKEN}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.id !== 'string') {
        cb(IdTypeError);
        return;
    }
    typeFetch(body, cb);
}
