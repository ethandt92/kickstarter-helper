const BodyUndefinedError = new Error('Body undefined');
const TermTypeError = new Error('Term type should be string');
const SortTypeError = new Error('Sort type should be string');
const PageTypeError = new Error('Page type should be number');

const ENDPOINT = "https://www.kickstarter.com"
// const PATH = "/projects/search.json"
const PATH = "/discover/advanced.json"

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?term=${params.term}&sort=${params.sort}&page=${params.page}`;
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.term !== 'string') {
        cb(TermTypeError);
        return;
    }
    if (typeof body.sort !== 'string') {
        cb(SortTypeError);
        return;
    }
    if (typeof body.page !== 'number') {
        cb(PageTypeError);
        return;
    }
    typeFetch(body, cb);
}