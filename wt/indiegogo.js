const BodyUndefinedError = new Error('Body undefined');
const PageTypeError = new Error('page type should be number');
const PerPageTypeError = new Error('per_page type should be number');
const QTypeError = new Error('q type should be string');

const ENDPOINT = "https://www.indiegogo.com"
const PATH = "/private_api/search"

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?page=${params.page}&per_page=${params.per_page}&q=${params.q}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.page !== 'number') {
        cb(PageTypeError);
        return;
    }
    if (typeof body.per_page !== 'number') {
        cb(PerPageTypeError);
        return;
    }
    if (typeof body.q !== 'string') {
        cb(QTypeError);
        return;
    }
    // typeNative(body.site, cb);
    // typeRequest(body.site, cb);
    // typeUnirest(body.site, cb);
    typeFetch(body, cb);
}
