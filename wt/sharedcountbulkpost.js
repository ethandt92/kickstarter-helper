const BodyUndefinedError = new Error('Body undefined');

const ENDPOINT = "https://free.sharedcount.com"
const PATH = "/bulk"
const API_KEY = "b488e797f58ed208e4ba1a9c7f4d493b81727663"

const fetch = require('isomorphic-fetch');
function typeFetch(body, cb) {
    fetch(`${ENDPOINT}${PATH}?apikey=${API_KEY}`, {
      method: 'POST',
      body: body
    })
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    typeFetch(context.body_raw, cb);
}