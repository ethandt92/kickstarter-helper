const BodyUndefinedError = new Error('Body undefined');
const SiteTypeError = new Error('Site type should be string');

const ENDPOINT = "https://www.alexa.com"
const PATH = "/find-similar-sites/data"

const fetch = require('isomorphic-fetch');
function typeFetch(site, cb) {
    const url = `${ENDPOINT}${PATH}?site=${site}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.site !== 'string') {
        cb(SiteTypeError);
        return;
    }
    // typeNative(body.site, cb);
    // typeRequest(body.site, cb);
    // typeUnirest(body.site, cb);
    typeFetch(body.site, cb);
}
