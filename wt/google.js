const google = require('googleapis');
const customsearch = google.customsearch('v1');

const CX = '000292445434511738025:met1igt5dew';
const API_KEY = 'AIzaSyDF4AFm1HSB_3-Sfgxp3HryHynaTO7QB4Q';

function search(q, opts, cb) {
    if (!opts)
        opts = {}
    var params = {
        cx: CX,
        auth: API_KEY,
        q: q
    }
    if (opts.start)
        params.start = opts.start;
    customsearch.cse.list(params, function (err, resp) {
        if (err) {
            cb(err);
            return;
        }
        cb(null, resp);
    });
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    search(body.q, {start: body.start}, cb);
}