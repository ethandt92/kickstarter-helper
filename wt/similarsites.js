const BodyUndefinedError = new Error('Body undefined');
const QTypeError = new Error('q type should be string');

const ENDPOINT = "https://s550.similarsites.com"
const PATH = "/related"

const fetch = require('isomorphic-fetch');
function typeFetch(q, cb) {
    const url = `${ENDPOINT}${PATH}?s=5501&md=18&q=${q}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.q !== 'string') {
        cb(QTypeError);
        return;
    }
    // typeNative(body.site, cb);
    // typeRequest(body.site, cb);
    // typeUnirest(body.site, cb);
    typeFetch(body.q, cb);
}
