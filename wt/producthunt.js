const BodyUndefinedError = new Error('Body undefined');

const algoliasearch = require('algoliasearch');

const APP_ID = '0H4SMABBSG';
const API_KEY = '9670d2d619b9d07859448d7628eea5f3';
const INDEX_NAME = 'Post_production';
const client = algoliasearch(APP_ID, API_KEY);
const index = client.initIndex(INDEX_NAME);

function search(q, opts, cb) {
    index.search(q, {
        hitsPerPage: opts.hitsPerPage || 20
    }, function searchDone(err, content) {
        if (err) {
            cb(err);
            return;
        }
        cb(null, content);
    })

}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    search(body.q, {hitsPerPage: body.hitsPerPage}, cb);
}