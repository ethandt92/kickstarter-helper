
const BodyUndefinedError = new Error('Body undefined');
const FromTypeError = new Error('From type should be number');
const QTypeError = new Error('q type should be string');

const ENDPOINT = "https://idb.buzzstream.com"
const PATH = "/search/json"

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?from=${params.from}&q=${params.q}`
    console.log(url)
    fetch(url)
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.from !== 'number') {
        cb(FromTypeError);
        return;
    }
    if (typeof body.q !== 'string') {
        cb(QTypeError);
        return;
    }
    typeFetch(body, cb);
}
