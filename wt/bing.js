
const BodyUndefinedError = new Error('Body undefined');
const QTypeError = new Error('q type should be string');
const CountTypeError = new Error('Count type should be number');
const OffsetTypeError = new Error('Offset type should be number');

const ENDPOINT = "https://api.cognitive.microsoft.com"
const PATH = "/bing/v5.0/search"
const KEY = 'e7d6336e41904b28b1d44124c7aefb38'
const headers = {
    'Ocp-Apim-Subscription-Key': KEY
}

const fetch = require('isomorphic-fetch');
function typeFetch(params, cb) {
    const url = `${ENDPOINT}${PATH}?q=${params.q}&count=10&offset=0`
    console.log(url)
    fetch(url, {headers})
        .then(response => response.json())
        .then(data => cb(null, data))
        .catch(err => cb(err));
}

module.exports = function (context, cb) {
    if (typeof context.body_raw !== 'string') {
        cb(BodyUndefinedError);
        return;
    }
    const body = JSON.parse(context.body_raw);
    if (typeof body.q !== 'string') {
        cb(QTypeError);
        return;
    }
    if (typeof body.count !== 'number') {
        cb(CountTypeError);
        return;
    }
    if (typeof body.offset !== 'number') {
        cb(OffsetTypeError);
        return;
    }
    typeFetch(body, cb);
}
